  uri = URI.parse("redis://localhost:6379")
  REDIS = Redis.new(host: uri.host, port: uri.port, password: uri.password)
  Resque.redis = Redis.new(host: uri.host, port: uri.port, password: uri.password)

# == Schema Information
#
# Table name: pages
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)
#  slug       :string(255)
#  content    :text
#  menu_order :integer(4)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Page < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  attr_accessible :title, :content, :menu_order
  validates_presence_of :title, :slug, :content, :menu_order
  validates_length_of :title, in: 1..255
  validates_numericality_of :menu_order
  scope :ordered, order('menu_order')
end

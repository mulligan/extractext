# == Schema Information
#
# Table name: coupon_subscription_plans
#
#  id                   :integer(4)      not null, primary key
#  coupon_id            :integer(4)
#  subscription_plan_id :integer(4)
#  created_at           :datetime        not null
#  updated_at           :datetime        not null
#

class CouponSubscriptionPlan < ActiveRecord::Base
  attr_accessible :coupon, :subscription_plan, :coupon_id, :subscription_plan_id
  validates_presence_of :coupon_id, :subscription_plan_id
  belongs_to :coupon
  belongs_to :subscription_plan
end


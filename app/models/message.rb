class Message
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :name, :email, :subject, :body

  validates :name, :email, :subject, :body, :presence => true
  validates_presence_of  :email
  validates_length_of :email, in: 1..255
  validates_format_of :email, with: /\A.+@.+\..+\Z/
  def initialize(attributes = { })
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

end

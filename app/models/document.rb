# == Schema Information
#
# Table name: documents
#
#  id         :integer(4)      not null, primary key
#  title      :string(255)
#  user_id    :integer(4)
#  content    :text
#  notes      :text
#  remind_at  :datetime
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Document < ActiveRecord::Base
  belongs_to :user
  attr_accessible  :title, :notes,:remind_at
end

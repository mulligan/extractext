class InvoiceDecorator
  def initialize(invoice)
    @invoice = invoice
  end

  def greeting
    if @invoice.attempted && @invoice.paid
      "Thank you for your payment!"
    elsif @invoice.attempted
      "Payment Over Due. Please update your credit card information as soon possible."
    else
      "Information Only. No action required."
    end
  end

  def subscription_name
    @invoice.lines.subscriptions[0].plan.name
  end

  def subscription_amount
    if @invoice.lines.subscriptions[0].amount > 0
      number_to_currency(@invoice.lines.subscriptions[0].amount.to_f / 100)
    else
      "$0.00"
    end

  end

  def next_invoice
    Time.at(@invoice.lines.subscriptions[0].period.end.to_i).to_date.to_formatted_s(:long_ordinal)
  end

end

class ErrorMailer < ActionMailer::Base
  default from: "transcribe@extractext.com"

  def no_attachment(email,title)
    @title = title
    mail to: "#{email}", subject: "#{title}"
  end

  def over_limit(email,title)
    @title = title
    mail to: "#{email}", subject: "Over Limit for Free Account"
  end
end

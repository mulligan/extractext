class ReminderMailer < ActionMailer::Base
  default from: "transcribe@extractext.com"

  def sanitize_filename(filename)
    filename.strip
    # Strip out the non-ascii character
    filename.gsub!(/[^0-9A-Za-z.\-]/, '_')
    filename
  end

  def kindle_dispatch(doc_id)
    convert_kindle = User.find(Document.find(doc_id).user_id).convert_kindle
    convert_kindle ? @title = "Convert" : @title = Document.find(doc_id).title
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.pdf"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.pdf")
    mail to: "#{User.find(Document.find(doc_id).user_id).kindle_email}", subject: "#{@title}"
  end

  def deliver_to_user(doc_id)
    @note_header = String.new
    @note_header << "Your original email included the following notes:" if !Document.find(doc_id).notes.nil? && !Document.find(doc_id).notes.strip.blank?
    @notes = String.new
    @notes << "\"#{Document.find(doc_id).notes}\"" if !Document.find(doc_id).notes.nil? && !Document.find(doc_id).notes.strip.blank?

    @title = Document.find(doc_id).title
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.pdf"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.pdf")
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.txt"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.txt")
    mail to: "#{User.find(Document.find(doc_id).user_id).email}", subject: "Reminder \"#{@title}\""
  end


end

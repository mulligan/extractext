class NotificationsMailer < ActionMailer::Base

  default :from => "transcribe@extractext.com"
  default :to => "ryan@rcm-tech.com"

  def new_message(message)
    @message = message
    mail(:subject => "New Contact #{message.subject}")
  end

  def lag_mail(process)
    @process = process
    mail(:subject => "Lag Mail")
  end

end

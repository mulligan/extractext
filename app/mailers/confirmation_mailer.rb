class ConfirmationMailer < ActionMailer::Base
  default from: "transcribe@extractext.com"

  def sanitize_filename(filename)
    filename.strip
    # Strip out the non-ascii character
    filename.gsub!(/[^0-9A-Za-z.\-]/, '_')
    filename
  end


  def subscriber_reminder(reminder,doc_id)
    evernote_enabled = !User.find(Document.find(doc_id).user_id).evernote_email.nil? && !User.find(Document.find(doc_id).user_id).evernote_email.strip.blank?
    kindle_enabled   = !User.find(Document.find(doc_id).user_id).kindle_email.nil? && !User.find(Document.find(doc_id).user_id).kindle_email.strip.blank?
    @evernote_enabled = String.new
    @evernote_enabled << " It has also been dispatched to your Evernote Account." if evernote_enabled
    @kindle_enabled = String.new
    @kindle_enabled << " the document will be sent to your kindle," if kindle_enabled
    @title = Document.find(doc_id).title
    @reminder = reminder
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.pdf"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.pdf")
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.txt"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.txt")
    mail to: "#{User.find(Document.find(doc_id).user_id).email}", subject: "#{@title}"
  end

  def subscriber_no_reminder(doc_id)
    evernote_enabled = !User.find(Document.find(doc_id).user_id).evernote_email.nil? && !User.find(Document.find(doc_id).user_id).evernote_email.strip.blank?
    kindle_enabled   = !User.find(Document.find(doc_id).user_id).kindle_email.nil? && !User.find(Document.find(doc_id).user_id).kindle_email.strip.blank?
    @evernote_enabled = String.new
    @evernote_enabled << " and dispatched to your Evernote Account" if evernote_enabled
    @title  = Document.find(doc_id).title
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.pdf"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.pdf")
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.txt"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.txt")
    mail to: "#{User.find(Document.find(doc_id).user_id).email}", subject: "#{@title}"
  end


  def evernote_dispatch(doc_id)
    @notes       = Document.find(doc_id).notes
    title        = Document.find(doc_id).title
    notebook_tag = String.new
    evernote_nb  = User.find(Document.find(doc_id).user_id).evernote_notebook
    tags         = User.find(Document.find(doc_id).user_id).evernote_tags
    tag_list     = String.new
    if !tags.nil? || !tags.strip.blank?
      tags.split(",").each { |tag| tag_list << "##{tag} "}
      tag_list.strip!
    end
    notebook_tag << "@#{evernote_nb}" if !evernote_nb.nil? && !evernote_nb.strip.blank?
    tags.split(",").each
    attachments["#{sanitize_filename(Document.find(doc_id).title)}.pdf"] = File.read("uploads/#{User.find(Document.find(doc_id).user_id).email}/#{doc_id}.pdf")
    mail to: "#{User.find(Document.find(doc_id).user_id).evernote_email}", subject: "#{title} #{notebook_tag} #{tag_list}"
  end

  def no_subscription(from)
    mail to: "#{from}", subject: "Extractext"
  end

  def drop_box_confirmation(title,email)
    @title = title
    mail to: "#{email}", subject: "#{title}"
  end


end

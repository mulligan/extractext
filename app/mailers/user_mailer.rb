class UserMailer < ActionMailer::Base
  default from: "transcribe@extractext.com"

  def invoice(user, invoice)
    @user = user
    @invoice = InvoiceDecorator.new(invoice)
    mail to: user.email, subject: 'Extractext Invoice'
  end

  def password_reset(user, token)
    @password_token = token
    mail to: user.email, subject: 'Password Reset'
  end

end

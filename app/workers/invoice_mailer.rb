class InvoiceMailer
  @queue = :invoice_mailer
  def self.perform(id,invoice)
    user = User.find(id)
    invoice = Stripe::StripeObject.construct_from(JSON.parse(invoice.to_json), "pk_5X367qpSxr7jgP4wR1vr8SRLUBzOo")
    puts invoice.inspect
    UserMailer.invoice(user, invoice).deliver
  end
end

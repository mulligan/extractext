// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap
//= require pages
//= require subscription_plans
//= require users
//= require main

$(document).ready(function(){
    if ($("#user_subscription_plan_id").val() == "6") {
        $("#card_number").attr("id", "card_number_disabled");
        $("#card_code").attr("id", "card_code_disabled");
        $("#card_month").attr("id", "card_month_disabled");
        $("#card_year").attr("id", "card_year_disabled");
        $("#card_number_disabled").hide();
        $("#card_code_disabled").hide();
        $("#card_month_disabled").hide();
        $("#card_year_disabled").hide();
        $('label[for="user_coupon_code"]').hide();
        $('#user_coupon_code').hide();
        $('#payment_details').hide();
    }

    $("#user_subscription_plan_id").change(function(){
        if($("#user_subscription_plan_id").val() != "6") {
            $("#card_number_disabled").attr("id", "card_number");
            $("#card_code_disabled").attr("id", "card_code");
            $("#card_month_disabled").attr("id", "card_month");
            $("#card_year_disabled").attr("id", "card_year");
            $("#card_number").show();
            $("#card_code").show();
            $("#card_month").show();
            $("#card_year").show();
            $('label[for="user_coupon_code"]').show();
            $('#user_coupon_code').show();
            $('#payment_details').show();
        }else{
            $("#card_number").attr("id", "card_number_disabled");
            $("#card_code").attr("id", "card_code_disabled");
            $("#card_month").attr("id", "card_month_disabled");
            $("#card_year").attr("id", "card_year_disabled");
            $("#card_number_disabled").hide();
            $("#card_code_disabled").hide();
            $("#card_month_disabled").hide();
            $("#card_year_disabled").hide();
            $('label[for="user_coupon_code"]').hide();
            $('#user_coupon_code').hide();
            $('#payment_details').hide();
        }
    });
});
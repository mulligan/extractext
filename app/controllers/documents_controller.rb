class DocumentsController < ApplicationController
  require "chronic"
  before_filter :require_admin, :except => [ :show,:edit ]
  def index
    @documents = Document.all
  end

  def show
    @document = Document.find(params[:id])
  end

  def new
    @document = Document.new
  end

  def edit
    @document  = Document.find(params[:id])
    if !@document.remind_at.nil?
      timezone = @document.user.timezone || "Pacific Time (US & Canada)"
      @remind_at =  @document.remind_at.in_time_zone("#{timezone}").strftime("%B #{@document.remind_at.day.ordinalize} at %l:%M %p")
    else
      @remind_at = ""
    end
  end

  def create
    @document = Document.new(params[:document])
    if @document.save
      redirect_to @document, notice: 'Document was successfully created.'
      render json: @document, status: :created, location: @document
    else
      render action: "new"
    end
  end

  def update
    @document = Document.find(params[:id])
    Time.zone = @document.user.timezone || "Pacific Time (US & Canada)"
    Chronic.time_class = Time.zone
    params[:document][:remind_at]  = Chronic.parse("#{params[:document][:remind_at]}")
    if @document.update_attributes(params[:document])
      redirect_to profile_path, notice: 'Document was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @document = Document.find(params[:id])
    @document.destroy
    redirect_to profile_url
  end
end

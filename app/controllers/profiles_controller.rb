class ProfilesController < ApplicationController
  include ActiveSupport
  def show
    @timezone = current_user.timezone || "Pacific Time (US & Canada)"
    @documents = current_user.documents.order("COALESCE(remind_at, '2100-01-01') ASC").page(params[:page]).per_page(5)
  end

  def edit
    @timezones = Array.new
    timezones = TimeZone.all
    timezones.each do |tz|
      tz = tz.to_s.gsub(/\(GMT.[0-9][0-9].[0-9][0-9]\)\ /,"")
      @timezones << tz
    end
    @timezones.sort!

    if params[:oauth_token]
      request_token = session[:request_token]
      access_token_object = request_token.get_access_token(:oauth_verifier => params[:oauth_token])
      token = access_token_object.token
      secret = access_token_object.secret
      current_user.update_attributes({ :dropbox_oauth => token,:dropbox_secret => secret})
    elsif current_user.dropbox_oauth
      client = Dropbox::API::Client.new :token => current_user.dropbox_oauth, :secret => current_user.dropbox_secret
      begin
        client.ls
      rescue Exception => exc
        current_user.update_attributes({ :dropbox_oauth => nil, :dropbox_secret => nil})
        flash[:notice] = "Please Reactivate Dropbox"
        redirect_to(:controller => 'profiles',:action => 'edit')
      end
    else
      consumer = Dropbox::API::OAuth.consumer(:authorize)
      request_token = consumer.get_request_token
      @authorize_url = request_token.authorize_url(:oauth_callback => "#{request.url}")
      session[:request_token] = request_token
    end
  end

  def update
    respond_to do |format|
      if current_user.update_attributes(params[:user])
        format.html { redirect_to profile_path, notice: 'Profile was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: current_user.errors, status: :unprocessable_entity }
      end
    end
  end
end

namespace :clear do
  desc "Clear Free User Conversion Count on Sunday"
  task :count => :environment do
    free_id    = SubscriptionPlan.find_by_vault_token("free").id
    free_users = User.find(:all,:conditions => ["subscription_plan_id = ? AND conversion_count >= 3", free_id])
    free_users.each do |user|
      user.update_attribute(:conversion_count,0)
      puts "Cleared count for #{user.email}"
    end
  end

  task :count_for_exceptions => :environment do
      evernote_guy = User.find_by_email("ctraganos@evernote.com")
      evernote_guy.update_attribute(:conversion_count,0)
      puts "Reset Evernote Guy's Count"
  end
end




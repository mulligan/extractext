namespace :lag do
  desc "Send Mail to admin if cron hasn't run in > 4 minutes"
  task :check_cron => :environment do
    NotificationsMailer.lag_mail("mail_checker").deliver!
  end

  desc "Send Mail to admin if cron hasn't run in > 4 minutes"
  task :remind_cron => :environment do
    NotificationsMailer.lag_mail("reminder").deliver!
  end

  desc "Send Mail to admin if cron hasn't run in > 4 minutes"
  task :dropbox_cron => :environment do
    NotificationsMailer.lag_mail("dropbox").deliver!
  end
end




namespace :convert do
  desc "Check and Convert from Dropbox Accounts"
  task :dropbox => :environment do
    enabled_accounts = User.find(:all,:conditions => ["dropbox_oauth IS NOT NULL"])
    enabled_accounts.each do |user|
      client = Dropbox::API::Client.new(:token  => user.dropbox_oauth, :secret => user.dropbox_secret)
      processor = DropboxProcessor.new(client,user)
      processor.go
    end
  end
end


namespace :mail do
  desc "Check gmail"
  task :check => :environment do
    require "net/pop"
    Net::POP3.enable_ssl(OpenSSL::SSL::VERIFY_NONE)
    Net::POP3.start('pop.gmail.com', 995, "transcribe@extractext.com", "M!tten#2") do |pop|
      if pop.mails.empty?
        puts 'No mail.'
      else
        pop.each_mail {  |mail| MailHandler.receive(mail.pop) }
      end
    end
  end
end

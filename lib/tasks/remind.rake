namespace :remind do
  desc "Send Reminders to Cell and Kindle"
  task :send => :environment do
    require 'googlevoiceapi'
    GV_API = GoogleVoice::Api.new('transcribe@extractext.com', 'M!tten#2')
    @documents = Document.find(:all,:conditions => ["remind_at IS NOT NULL and remind_at > ?", 1.day.ago])
    @documents.each do |d|
      user = User.find(d.user_id)
      timezone = user.timezone || "Pacific Time (US & Canada)"

      if Time.now.in_time_zone(timezone) >= d.remind_at.in_time_zone(timezone)
        # SMS, & KINDLE ENABLED
        if (!user.cell_number.nil? && !user.cell_number.strip.blank?) && (!user.kindle_email.strip.blank?)
          # Send Email to User
          puts "Sending email Reminder"
          ReminderMailer.deliver_to_user(d.id).deliver!
          # Send SMS
          puts "SMS Reminder"
          GV_API.sms("#{user.cell_number}","This is a reminder for \"#{d.title}\". The PDF version has been delivered to your Kindle.")
          # Dispatch to Kindle
          puts "Sending to Kindle"
          ReminderMailer.kindle_dispatch(d.id).deliver!
        elsif (!user.cell_number.nil? && !user.cell_number.strip.blank?) && (user.kindle_email.strip.blank?)
          puts "Sending email reminder"
          # Send Email to User
          ReminderMailer.deliver_to_user(d.id).deliver!
          puts "Sending SMS reminder"
          # Send SMS
          GV_API.sms("#{user.cell_number}","This is a reminder for \"#{d.title}\". You will receive an email containing original notes and conversions.")
        elsif (user.cell_number.nil? || user.cell_number.strip.blank?) && (!user.kindle_email.strip.blank?)
          # Send Email to User
          puts "Sending Email Reminder"
          ReminderMailer.deliver_to_user(d.id).deliver!
          # Dispatch to Kindle
          puts "Dispatching to Kindle"
          ReminderMailer.kindle_dispatch(d.id).deliver!
        elsif (user.cell_number.nil? || user.cell_number.strip.blank?) && (user.kindle_email.strip.blank?)
          # Send Email to User
          puts "Sending Email Reminder"
          ReminderMailer.deliver_to_user(d.id).deliver!
        end
        d.update_attribute(:remind_at,nil)
      end
    end
  end
end




class Converter
  def initialize(user,attachment)
    @user    = user
    @attachment = attachment
    @target     = "uploads/#{@user.email}/#{sanitize_filename(@attachment.filename)}"
    get_base_64
  end

  def sanitize_filename(filename)
    filename.strip
    # Strip out the non-ascii character
    filename.gsub!(/[^0-9A-Za-z.\-]/, '_')
    filename
  end

  def get_base_64
    parts    = @attachment.to_s.split("\n")
    # GET THE BASE64 CODE FOR THE DOCUMENT
    @start_base = 0
    content = String.new
    parts.each do |part|
      @start_base = 1 if part.strip == ""
      content << part if @start_base > 1
      @start_base += 1 if @start_base != 0
    end
    extension = @attachment.filename[-3,3].downcase
    extension == "pdf" ? save_initial_pdf(content) : save_initial_image(content)
  end

  def save_initial_pdf content
    # OPEN PDF IN BINARY MODE
    File.open(@target, "wb+") do |f|
      # UNPACK BASE64 TO GET PDF SOURCE AND SAVE AS ORIGINAL FILENAME
      f.write(content.unpack("m")[0])
    end
    puts "Saved #{@attachment.filename}"
    pdf_to_tifs
  end

  def pdf_to_tifs
    puts "splitting pdf to tifs"
    # Split into tif pages
    system "gs -r300x300 -sDEVICE=tiffgray -sOutputFile=uploads/#{@user.email}/ocr_rcm_%02d.tif -dBATCH -dNOPAUSE #{@target}"
    remove_original_file
  end

  def save_initial_image content
    File.open("#{@target}", 'wb') {|f| f.write(Base64.decode64(content))}
    img_to_tif
  end

  def img_to_tif
    puts "converting #{@attachment.filename[-3,3].downcase} to tif"
    system "convert #{@target} uploads/#{@user.email}/ocr_rcm_01.tif"
    remove_original_file
  end

  def remove_original_file
    system("rm -rf #{@target}")
  end

  def ocr
    tifs = Dir.new("uploads/#{@user.email}").grep(/^ocr_rcm_.*\.tif$/).sort
    tifs.each do |tif|
      puts "Performing OCR on #{tif}"
      system "/usr/local/bin/tesseract uploads/#{@user.email}/#{tif} uploads/#{@user.email}/#{tif[0..-5]} -l eng hocr"
      # Text Version
      system "/usr/local/bin/tesseract uploads/#{@user.email}/#{tif} uploads/#{@user.email}/#{tif[0..-5]} -l eng"
      create_searchable_pdf(tif[0..-5])
    end
  end

  def create_searchable_pdf base
    puts "Creating searchable pdf for #{base}.tif"
    system "/usr/local/bin/hocr2pdf -i uploads/#{@user.email}/#{base}.tif -o uploads/#{@user.email}/#{base}_searchable.pdf < uploads/#{@user.email}/#{base}.html"
  end

  def merge_pdfs
    puts "Merging PDFs"
    pdfs = Dir.new("uploads/#{@user.email}").grep(/^ocr_rcm_.*\_searchable.pdf$/).sort
    filestring = String.new
    pdfs.each { |pdf| filestring << "uploads/#{@user.email}/#{pdf} "}
    system "gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=uploads/#{@user.email}/ocr_rcm_merged.pdf #{filestring}"
    pdfs.each { |pdf| system("rm uploads/#{@user.email}/#{pdf}")}
    system("rm uploads/#{@user.email}/*.html")
    puts "Merge completed for email #{@user.email}"
  end

end

class DropboxProcessor
  require 'googlevoiceapi'
  GV_API = GoogleVoice::Api.new('transcribe@extractext.com', 'M!tten#2')
  def initialize client,user
    @client = client
    @user   = user
    @target = "uploads/#{@user.email}/"
    # In case it's not already there
    system("mkdir #{@target}") unless File.directory? @target
  end

  def go
    # Cycle through user's "Extractext" Folder
    @client.ls.each do |file|
      if (!@user.nil? && !@user.subscription_plan_id.nil?) && (@user.subscription_plan.vault_token != "free" || (@user.subscription_plan.vault_token == "free" && (@user.conversion_count < 3 || @user.is_admin?)))
        check_type file
        ocr
        merge_pdfs
      end
    end
  end

  def check_type file
    @file = file unless already_processed? file
    if file.mime_type.match('image')
      save_image file unless already_processed?(file)
    elsif file.mime_type == "application/pdf"
      save_pdf file unless already_processed?(file)
    end
  end

  def already_processed? file
    raw_title = file.path.gsub("/","")
    extension = File.extname(raw_title)
    title     = raw_title.gsub(extension,"")
    begin
      match = @client.find("#{title}.txt")
      return true
    rescue Exception => exc
      @title = title
      return false
    end
  end

  def save_image file
    puts "processing image"
    filename = file.path.gsub("/","")
    file_content = @client.download filename
    File.open("#{@target}#{filename}",'wb'){|f| f.write(file_content)}
    img_to_tif filename
  end

  def save_pdf file
    puts "processing pdf"
    filename = file.path.gsub("/","")
    file_content = @client.download filename
    File.open("#{@target}#{filename}",'wb'){|f| f.write(file_content)}
    pdf_to_tifs filename
  end

  def img_to_tif filename
    puts "converting #{filename} to tif"
    system "convert #{@target}#{filename} uploads/#{@user.email}/ocr_rcm_01.tif"
    system "rm #{@target}#{filename} -rf"
  end

  def pdf_to_tifs filename
    puts "splitting pdf to tifs"
    # Split into tif pages
    system "gs -r300x300 -sDEVICE=tiffgray -sOutputFile=uploads/#{@user.email}/ocr_rcm_%02d.tif -dBATCH -dNOPAUSE #{@target}#{filename}"
    system "rm -rf #{@target}#{filename}"
  end

  def ocr
    tifs = Dir.new("uploads/#{@user.email}").grep(/^ocr_rcm_.*\.tif$/).sort
    tifs.each do |tif|
      puts "Performing OCR on #{tif}"
      system "/usr/local/bin/tesseract uploads/#{@user.email}/#{tif} uploads/#{@user.email}/#{tif[0..-5]} -l eng hocr"
      # Text Version
      system "/usr/local/bin/tesseract uploads/#{@user.email}/#{tif} uploads/#{@user.email}/#{tif[0..-5]} -l eng"
      create_searchable_pdf(tif[0..-5])
    end
  end


  def create_searchable_pdf base
    puts "Creating searchable pdf for #{base}.tif"
    system "/usr/local/bin/hocr2pdf -i uploads/#{@user.email}/#{base}.tif -o uploads/#{@user.email}/#{base}_searchable.pdf < uploads/#{@user.email}/#{base}.html"
  end

  def merge_pdfs
    pdfs = Dir.new("uploads/#{@user.email}").grep(/^ocr_rcm_.*\_searchable.pdf$/).sort
    unless pdfs.empty?
      puts "Merging PDFs"
      filestring = String.new
      pdfs.each { |pdf| filestring << "uploads/#{@user.email}/#{pdf} "}
      system "gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=uploads/#{@user.email}/ocr_rcm_merged.pdf #{filestring}"
      pdfs.each { |pdf| system("rm uploads/#{@user.email}/#{pdf}")}
      system("rm uploads/#{@user.email}/*.html")
      puts "Merge completed for email #{@user.email}"
      save_document @title
    end
  end

  def save_document title
    content = String.new
    recog_txts = Dir.new("uploads/#{@user.email}").grep(/^ocr_rcm_.*\.txt$/).sort
    recog_txts.each { |rec| content << File.open("uploads/#{@user.email}/#{rec}", "rb").read}
    @document = Document.create do |d|
      d.title      = title
      d.content    = content
      d.user_id = @user.id
    end
    file = File.open("uploads/#{@user.email}/#{@document.id}.txt","w")
    content = @document.content.force_encoding("UTF-8")
    file.write(content)
    file.close
    system("convert uploads/#{@user.email}/ocr_rcm_01.tif uploads/#{@user.email}/#{@document.id}.jpg")
    system("mv uploads/#{@user.email}/ocr_rcm_merged.pdf uploads/#{@user.email}/#{@document.id}.pdf")
    system("rm uploads/#{@user.email}/ocr_rcm*")
    migrate_to_dropbox title
  end

  def migrate_to_dropbox title
    puts "Sending to dropbox"
    @client.upload "#{title}.txt", File.open("#{@target}#{@document.id}.txt").read
    @client.upload "#{title}.pdf", File.open("#{@target}#{@document.id}.pdf","rb").read
    send_confirmations title
  end

  def send_confirmations title
    conversion_count = @user.conversion_count
    @user.update_attribute(:conversion_count,conversion_count += 1)
    GV_API.sms("#{@user.cell_number}", "Transcription completed for \"#{title}\" and results uploaded to your dropbox account. If original file was pdf, it has been updated to be searchable.") if (!@user.cell_number.nil? && !@user.cell_number.strip.blank?)
    ConfirmationMailer.evernote_dispatch(@document.id).deliver! if !@user.evernote_email.nil? && !@user.evernote_email.strip.blank?
    ConfirmationMailer.drop_box_confirmation(title,@user.email).deliver!
  end
end

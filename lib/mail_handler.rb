class MailHandler < ActionMailer::Base
  require 'chronic'
  require 'googlevoiceapi'
  GV_API = GoogleVoice::Api.new('transcribe@extractext.com', 'M!tten#2')
  def receive(email)
    from = email.from[0]
    subject  = email.subject
    # Is there a file attachment?
    if email.attachments.first
      puts "Has Attachment"
      puts email.attachments.first.inspect
      body     = email.text_part.body.decoded
      # Find account for saving file if there is an attachment
      @user = User.find_by_email(from)
      if !@user.nil?
        system("mkdir uploads/#{@user.email}") unless File.directory? "uploads/#{@user.email}"
      end
      # Check for active subscription
      if (!@user.nil? && !@user.subscription_plan_id.nil?) && (@user.subscription_plan.vault_token != "free" || (@user.subscription_plan.vault_token == "free" && (@user.conversion_count < 3 || @user.is_admin?)))
        converter = Converter.new(@user,email.attachments.first)
        converter.ocr
        converter.merge_pdfs
        parse_message body,subject
        # Was reminder absent or not parsable?
        @document.remind_at.nil? ? no_reminder = true : no_reminder = false
        # Send confirmation and results
        if no_reminder # different email for no reminder set
          # Send confirmation (Subscribed with no reminder)
          puts "Sending No Reminder E-mail"
          ConfirmationMailer.subscriber_no_reminder(@document.id).deliver!
          GV_API.sms("#{@user.cell_number}", "Transcription completed for \"#{@document.title}\" and results delivered to #{@user.email}") if (!@user.cell_number.nil? && !@user.cell_number.strip.blank?)
        else
          # Send confirmation (Subscribed with reminder)
          puts "Sending Reminder E-mail"
          ConfirmationMailer.subscriber_reminder(@document.remind_at.strftime("%A, %B #{@document.remind_at.day.ordinalize}, %Y at %l:%M %p %Z"),@document.id).deliver!
          GV_API.sms("#{@user.cell_number}", "Reminder for \"#{@document.title}\" set for #{@document.remind_at.strftime("%A, %B #{@document.remind_at.day.ordinalize}, %Y at %l:%M %p %Z")}") if  !@user.cell_number.nil? && !@user.cell_number.strip.blank?
        end
        user_conversion_count = @user.conversion_count + 1
        @user.update_attribute(:conversion_count,user_conversion_count) if @user.subscription_plan.vault_token == "free" && !@user.is_admin?
        ConfirmationMailer.evernote_dispatch(@document.id).deliver! if !@user.evernote_email.nil? && !@user.evernote_email.strip.blank?
      elsif  (!@user.nil? && !@user.subscription_plan_id.nil?) && (@user.subscription_plan.vault_token == "free" && @user.conversion_count >= 3)
        puts "Sending Over-Limit message to #{from}"
        ErrorMailer.over_limit(from,subject).deliver!
      else
        # Not a subscriber
        puts "Sending Rejection letter to #{from}"
        ConfirmationMailer.no_subscription(from).deliver!
      end
    else
      puts "no attachment"
      # Send no attachment error message
      ErrorMailer.no_attachment(from,subject).deliver!
    end
  end

  def parse_message body,subject
    body_line_count = body.split(/\n/).count
    countdown       = body_line_count
    notes           = String.new
    reminder_line   = String.new
    body.split(/\n/).each do |line|
      if countdown < body_line_count
        notes << "#{line}\n"
      else
        reminder_line << line
      end
      countdown -= 1
    end
    save_document(reminder_line,notes,subject)
  end

  def save_document(reminder,notes,subject)
    !@user.timezone.nil? ? Time.zone = @user.timezone : Time.zone = "Pacific Time (US & Canada)"
    Chronic.time_class = Time.zone
    content = String.new
    recog_txts = Dir.new("uploads/#{@user.email}").grep(/^ocr_rcm_.*\.txt$/).sort
    recog_txts.each { |rec| content << File.open("uploads/#{@user.email}/#{rec}", "rb").read}
    @document = Document.create do |d|
      d.title      = subject
      d.content    = content
      d.notes      = notes
      d.user_id = @user.id
      d.remind_at  = Chronic.parse("#{reminder}")
    end
    file = File.open("uploads/#{@user.email}/#{@document.id}.txt","w")
    content = @document.content.force_encoding("UTF-8")
    file.write(content)
    file.close
    system("convert uploads/#{@user.email}/ocr_rcm_01.tif uploads/#{@user.email}/#{@document.id}.jpg")
    system("mv uploads/#{@user.email}/ocr_rcm_merged.pdf uploads/#{@user.email}/#{@document.id}.pdf")
    system("rm uploads/#{@user.email}/ocr_rcm*")
  end


end

class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title
      t.integer :user_id
      t.text :content
      t.text :notes
      t.datetime :remind_at

      t.timestamps
    end
  end
end

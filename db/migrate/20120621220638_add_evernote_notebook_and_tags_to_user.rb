class AddEvernoteNotebookAndTagsToUser < ActiveRecord::Migration
  def change
    add_column :users, :evernote_notebook, :string

    add_column :users, :evernote_tags, :string

  end
end

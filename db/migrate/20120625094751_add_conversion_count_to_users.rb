class AddConversionCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :conversion_count, :integer, :default => 0

  end
end

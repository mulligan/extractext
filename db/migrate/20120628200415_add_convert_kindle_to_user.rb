class AddConvertKindleToUser < ActiveRecord::Migration
  def change
    add_column :users, :convert_kindle, :boolean, :default => false

  end
end

class ChangeDefaultValueForConvertKindle < ActiveRecord::Migration
  def up
    change_column :users, :convert_kindle, :boolean, :default => true
  end

  def down
    change_column :users, :convert_kindle, :boolean, :default => false
  end
end

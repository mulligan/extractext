# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "nkallen-cache-money"
  s.version = "0.2.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Nick Kallen"]
  s.date = "2008-11-24"
  s.description = "Cache utilities."
  s.email = "nick@twitter.com"
  s.homepage = "http://github.com/nkallen/cache-money"
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.11"
  s.summary = "Write-through and Read-through Cacheing for ActiveRecord"

  if s.respond_to? :specification_version then
    s.specification_version = 2

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activerecord>, [">= 2.2.0"])
      s.add_runtime_dependency(%q<activesupport>, [">= 2.2.0"])
      s.add_runtime_dependency(%q<memcache-client>, [">= 1.5.0"])
    else
      s.add_dependency(%q<activerecord>, [">= 2.2.0"])
      s.add_dependency(%q<activesupport>, [">= 2.2.0"])
      s.add_dependency(%q<memcache-client>, [">= 1.5.0"])
    end
  else
    s.add_dependency(%q<activerecord>, [">= 2.2.0"])
    s.add_dependency(%q<activesupport>, [">= 2.2.0"])
    s.add_dependency(%q<memcache-client>, [">= 1.5.0"])
  end
end

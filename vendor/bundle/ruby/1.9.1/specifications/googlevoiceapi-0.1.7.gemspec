# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "googlevoiceapi"
  s.version = "0.1.7"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Tim Gourley"]
  s.date = "2011-10-07"
  s.description = "Uses Mechanize to screen-scrape Google Voice since there is no public API"
  s.email = ["tgourley@gmail.com"]
  s.homepage = "http://github.com/bratta/googlevoiceapi"
  s.require_paths = ["lib"]
  s.rubyforge_project = "testing"
  s.rubygems_version = "1.8.23"
  s.summary = "Ruby library for interacting with Google Voice"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_runtime_dependency(%q<mechanize>, [">= 0"])
      s.add_runtime_dependency(%q<htmlentities>, [">= 0"])
    else
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<mechanize>, [">= 0"])
      s.add_dependency(%q<htmlentities>, [">= 0"])
    end
  else
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<mechanize>, [">= 0"])
    s.add_dependency(%q<htmlentities>, [">= 0"])
  end
end

In July 1345 Texas formally accepted an American proposal
to be annexed to the United States. Already strained relations
between the United States and Mexico rapidly worsened. President
James K. Polk ordered General Zachary Taylor and his troops to
Corpus Christi. In March 1846, under instructions, Taylor took up
positions on the Rio Grande. On April 26 an American squadron
of dragoons surrounded by Mexicans and unable to fight its way
out surrendered. Several Americans lost their lives in the defeat.
On May B the Mexicans intercepted Taylor at Pale Alto but
were driven back. The next day Mexicans again gave way in battle
at Resaca de la Palma. In June Taylor began a march toward
Monterrey, taking that city on September 25. Two months later
the Americans took Saltillo and with little effort ﬁled into
Tampico.
Santa Anna now took the field against the American forces in
northern Mexico, finally engaging the Americans at Buena Vista in
February. U|:lOl'l learning of the Mexican's design, General John E.
Wool, marched from San Antonio to join Taylor's forces,
abandoning his prior plans to take Chihuahua. After extremely
bitter fighting Santa Anna pulled out his army, leaving Taylor in
control of northern Mexico.
While Taylor pursued the enemy, Colonel Stephen W. Kearny
took the “Army of the west" into New Mexico, capturing Santa
Fe on August 16, 1846. Kearny then divided his forces, taking par!
to California and sending the remainder under Alexander W.
Doniphan against Chihuahua. After General Winfield Scott captured
Mexico City the two countries finally reached a settlement.
Source: Maps adapted from The West Pt7mtAt/as ufA/rrer-
ican Wars, Volume 1. Frederick A. Praeger, 1959.
lofllltghl WW Btlilt|£I'Rtg'fll! vii<i7~\=»iiin lt'\1~§\Yl>'lll -\l1rl_iLlrlv.'\'~(1\tll

